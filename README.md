# Helm demo chart
- Create a helm chart `helm create mychart`
- helmignore
- Template functions
    * `include`
    Eg: `value: {{ include "mytemplate" . | lower | quote }}`
    * `required`
    Eg: `value: {{ required "A valid .Values.who entry required!" .Values.who }}`

- Commands
```
helm lint

helm template -f values.yaml mychart . --debug
OR
helm template -f values.yaml mychart . --debug  | kubectl apply --dry-run -f -

helm install -f values.yaml mychart .
helm get manifest mychart
```